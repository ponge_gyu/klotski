## Introduction
Thank you for the opportunity for me to doing the 2rd test.
I'm using Vue as a framework for this Number Klotski game.
## Installation
After clone the repository, install the composer and npm by running the script below:
- composer install
- npm install
- php artisan key:generate
## Running the development server
After installing the composer and npm, try to run the Laravel Development server by running the script below: 
- php artisan serve

## File Location
1. You can find the .vue and .js at resources/js
2. /components will store all .vue file.
3. For the .scss file can be found at resources/sass. I've commented the import bootstrap line, so there's no bootstrap css on the website.