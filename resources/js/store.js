export default{
    state:{
        scores:[]
    },
    mutations:{
        addToScores(state, payload){
            state.scores.push(payload);
        },
        setScores(state,payload){
            state.scores = payload;
        }
    },
    actions: {
        addToScores({commit, state}, payload){
            commit("addToScores",payload);
            state.scores = state.scores.sort((a,b) => a-b).slice(0,5);
            commit("setScores",state.scores);
            localStorage.setItem('scores', JSON.stringify(state.scores));
        },
    },
    getters:{
        scoresLength: (state) => state.scores.length,
        getHighScore(state){
            return function(){
                return state.scores;
                // return state.scores.reduce((result, item) => result || item.bookable.id == id,false);
            }
        }
    }
};