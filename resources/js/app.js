
// require('./bootstrap');
import router from "./routes";
import VueRouter from "vue-router";
import Vuex from "vuex";
import Vue from "vue";
import storeDefinition from "./store";
import { library } from '@fortawesome/fontawesome-svg-core'
import { faArrowLeft,faArrowRight,faArrowUp, faArrowDown } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'


library.add(faArrowLeft,faArrowRight,faArrowUp, faArrowDown)
Vue.component('font-awesome-icon', FontAwesomeIcon)

window.Vue = require('vue');

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
// Vue.component('example-2', require('./components/Example2.vue').default);
Vue.config.devtools = true;
Vue.use(VueRouter);
Vue.use(Vuex);
const store = new Vuex.Store(storeDefinition);

const app = new Vue({
    el: '#app',
    store,
    router,
});
